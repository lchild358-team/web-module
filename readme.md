# web-module
Inlementation of module pattern for loading the content dynamically on web site.

## Usage

### Include
```html
<script src="node_modules/web-module/web-module.js"></script>
```

### Define module
```javascript
module.define('module-name', (function(){

  // Return something
})());
```

### Get module
```javascript
const func = module.require('module-name');

// Do something with func
```

## Built-in modules

### load
Load html, javascript, css content dynamically.

#### Example:
```javascript
const load = module.require('load');

load('html-url')
.then((html)=>{
  // Do something with html
})
.load('script-url')
.then(()=>{
  // Do something after loading script
})
.load('style.css');
```

## Bugs or feature requests
Please contact to [email](mailto:lchild358@yahoo.com.vn)

## License
[ISC](https://opensource.org/licenses/ISC)
